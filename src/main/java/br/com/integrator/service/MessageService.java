package br.com.integrator.service;

import br.com.integrator.model.IntegrationModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.sql.Timestamp;
import java.util.Date;

@Service
public class MessageService {

    @Autowired
    private KafkaTemplate<String, IntegrationModel> kafkaTemplate;

    @Value("${iot.kafka.topic-name}")
    private String iotTopic;

    public void iotQueueSendMessage(IntegrationModel message) {

        Timestamp time = new Timestamp(new Date().getTime());
        String key = String.valueOf(time.getTime());

        ListenableFuture<SendResult<String, IntegrationModel>> future =
                kafkaTemplate.send(iotTopic, key, message);

        future.addCallback(new ListenableFutureCallback<SendResult<String, IntegrationModel>>() {

            @Override
            public void onSuccess(SendResult<String, IntegrationModel> result) {
                System.out.println("Sent message=[" + message + "]");
                System.out.println("Offset=[" + result.getRecordMetadata().offset() + "]");
            }

            @Override
            public void onFailure(Throwable ex) {
                System.out.println("Unable to send message=["
                        + message + "] due to : " + ex.getMessage());
            }
        });
    }

}
