package br.com.integrator.application;

import br.com.integrator.vEye.client.NatsStartupConnector;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "br.com.integrator")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public NatsStartupConnector natsStartupConnector(){
		return new NatsStartupConnector();
	}
}
