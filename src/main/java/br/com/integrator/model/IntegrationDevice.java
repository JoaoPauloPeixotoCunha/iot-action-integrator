package br.com.integrator.model;

import java.io.Serializable;
import java.util.Objects;

public class IntegrationDevice implements Serializable {

    private String msisdn;
    private String iccid;
    private String vendor;

    public IntegrationDevice() {
    }

    public IntegrationDevice(String msisdn, String iccid, String vendor) {
        this.msisdn = msisdn;
        this.iccid = iccid;
        this.vendor = vendor;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntegrationDevice that = (IntegrationDevice) o;
        return Objects.equals(msisdn, that.msisdn) && Objects.equals(iccid, that.iccid) && Objects.equals(vendor, that.vendor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(msisdn, iccid, vendor);
    }

    @Override
    public String toString() {
        return "Device{" +
                "msisdn='" + msisdn + '\'' +
                ", iccid='" + iccid + '\'' +
                ", vendor='" + vendor + '\'' +
                '}';
    }
}
