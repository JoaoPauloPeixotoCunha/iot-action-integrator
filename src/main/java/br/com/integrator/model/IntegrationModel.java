package br.com.integrator.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class IntegrationModel implements Serializable {

        private String externalIdentifier;
        private String partnerName;
        private Integer actionType;
        private String contractId;
        private List<IntegrationDevice> devices;
        private Long customerIdDevice;
        private Long customerIdRequest;
        private String vendor;
        private List<String> operators;
        private Integer blockDays;
        private String processData;

        public IntegrationModel() {
        }

        public IntegrationModel(String externalIdentifier, String partnerName, Integer actionType, String contractId, List<IntegrationDevice> devices, Long customerIdDevice, Long customerIdRequest, String vendor, List<String> operators, Integer blockDays, String processData) {
            this.externalIdentifier = externalIdentifier;
            this.partnerName = partnerName;
            this.actionType = actionType;
            this.contractId = contractId;
            this.devices = devices;
            this.customerIdDevice = customerIdDevice;
            this.customerIdRequest = customerIdRequest;
            this.vendor = vendor;
            this.operators = operators;
            this.blockDays = blockDays;
            this.processData = processData;
        }

        public String getExternalIdentifier() {
            return externalIdentifier;
        }

        public void setExternalIdentifier(String externalIdentifier) {
            this.externalIdentifier = externalIdentifier;
        }

        public String getPartnerName() {
            return partnerName;
        }

        public void setPartnerName(String partnerName) {
            this.partnerName = partnerName;
        }

        public Integer getActionType() {
            return actionType;
        }

        public void setActionType(Integer actionType) {
            this.actionType = actionType;
        }

        public String getContractId() {
            return contractId;
        }

        public void setContractId(String contractId) {
            this.contractId = contractId;
        }

        public List<IntegrationDevice> getDevices() {
            return devices;
        }

        public void setDevices(List<IntegrationDevice> devices) {
            this.devices = devices;
        }

        public Long getCustomerIdDevice() {
            return customerIdDevice;
        }

        public void setCustomerIdDevice(Long customerIdDevice) {
            this.customerIdDevice = customerIdDevice;
        }

        public Long getCustomerIdRequest() {
            return customerIdRequest;
        }

        public void setCustomerIdRequest(Long customerIdRequest) {
            this.customerIdRequest = customerIdRequest;
        }

        public String getVendor() {
            return vendor;
        }

        public void setVendor(String vendor) {
            this.vendor = vendor;
        }

        public List<String> getOperators() {
            return operators;
        }

        public void setOperators(List<String> operators) {
            this.operators = operators;
        }

        public Integer getBlockDays() {
            return blockDays;
        }

        public void setBlockDays(Integer blockDays) {
            this.blockDays = blockDays;
        }

        public String getProcessData() {
            return processData;
        }

        public void setProcessData(String processData) {
            this.processData = processData;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            IntegrationModel that = (IntegrationModel) o;
            return Objects.equals(externalIdentifier, that.externalIdentifier) && Objects.equals(partnerName, that.partnerName) && Objects.equals(actionType, that.actionType) && Objects.equals(contractId, that.contractId) && Objects.equals(devices, that.devices) && Objects.equals(customerIdDevice, that.customerIdDevice) && Objects.equals(customerIdRequest, that.customerIdRequest) && Objects.equals(vendor, that.vendor) && Objects.equals(operators, that.operators) && Objects.equals(blockDays, that.blockDays) && Objects.equals(processData, that.processData);
        }

        @Override
        public int hashCode() {
            return Objects.hash(externalIdentifier, partnerName, actionType, contractId, devices, customerIdDevice, customerIdRequest, vendor, operators, blockDays, processData);
        }

        @Override
        public String toString() {
            return "IntegrationModel{" +
                    "externalIdentifier='" + externalIdentifier + '\'' +
                    ", partnerName='" + partnerName + '\'' +
                    ", actionType=" + actionType +
                    ", contractId='" + contractId + '\'' +
                    ", devices=" + devices +
                    ", customerIdDevice=" + customerIdDevice +
                    ", customerIdRequest=" + customerIdRequest +
                    ", vendor='" + vendor + '\'' +
                    ", operators=" + operators +
                    ", blockDays=" + blockDays +
                    ", processData='" + processData + '\'' +
                    '}';
        }
}
