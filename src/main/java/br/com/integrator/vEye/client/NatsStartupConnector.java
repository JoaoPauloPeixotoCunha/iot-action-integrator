package br.com.integrator.vEye.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class NatsStartupConnector implements CommandLineRunner {

    @Autowired
    public NatsClient natsClient;

    @Override
    public void run(String... args) throws Exception {
        natsClient.subscribeAsync();
    }
}
