package br.com.integrator.vEye.client;

import br.com.integrator.model.IntegrationModel;
import br.com.integrator.service.MessageService;
import br.com.integrator.vEye.service.NatsToKafkaConvertService;
import io.nats.streaming.Options;
import io.nats.streaming.StreamingConnection;
import io.nats.streaming.StreamingConnectionFactory;
import io.nats.streaming.SubscriptionOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class NatsClient {

    private final static Logger log = LoggerFactory.getLogger(NatsClient.class);

    @Value("${veye.nats.consumer.uri}")
    private String uri;

    @Value("${veye.nats.topic.name}")
    private String topicName;

    @Value("${veye.nats.retry.timeout}")
    private Long retryTimeout;


    @Autowired
    private NatsToKafkaConvertService natsToKafkaConvertService;

    @Autowired
    private MessageService messageService;

    public StreamingConnection initConnection(String clientId) {
        try {
            Options options = new Options.Builder().clientId(clientId)
                    .natsUrl(uri)
                    .clusterId("veye-cluster")
                    .maxPubAcksInFlight(1).build();

            return new StreamingConnectionFactory(options).createConnection();
        } catch (IOException | InterruptedException ioe) {
            System.out.println("Unable to connect to NATS servers: " + uri);
            System.out.println("Retrying...");
            return null;
        }
    }

    public void subscribeAsync() {
        try {
            int retryCount = 0;
            StreamingConnection natsStreamingConnection = initConnection("algar_async_consumer");
            while (natsStreamingConnection == null && retryCount < 10) {
                try {
                    natsStreamingConnection = initConnection("algar_async_consumer");
                    if (natsStreamingConnection == null) {
                        Thread.sleep(retryTimeout);
                        retryCount++;
                    }
                    if(retryCount == 9){
                        throw new Exception("After 10 retries, the NATs servers didn't respond, aborting connection...");
                    }
                } catch (Exception e) {
                    throw new Exception(e.getMessage());
                }
            }
            System.out.println("Connected successfully to NATS servers: " + uri + " !");

            assert natsStreamingConnection != null;
            natsStreamingConnection.subscribe(topicName, "group05", message -> {
                String str = new String(message.getData(), StandardCharsets.UTF_8);
                IntegrationModel integrationModel;
                try {

                    integrationModel = natsToKafkaConvertService.convertFromVeyeToIot(str);
                    System.out.println("JSON Message Received: " + str);
                    System.out.println("Kafka Message Transfered: " + integrationModel.toString());

                    messageService.iotQueueSendMessage(integrationModel);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, new SubscriptionOptions.Builder().durableName("algar_subscribe").build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String subscribeSync() throws InterruptedException {
        AtomicReference<String> str = new AtomicReference<>("");
        try {
            StreamingConnection natsStreamingConnection = initConnection("algar_sync_consumer");

            natsStreamingConnection.subscribe(topicName,
                    "group05",
                    message -> {
                        str.set(new String(message.getData(), StandardCharsets.UTF_8));
                        System.out.printf("Received a message: %s\n", str.get());
                    },
                    new SubscriptionOptions.Builder().deliverAllAvailable().build());

            natsStreamingConnection.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return str.get();
    }

}
