package br.com.integrator.vEye.service;

import br.com.integrator.model.IntegrationModel;
import br.com.integrator.service.MessageService;
import br.com.integrator.vEye.model.VEyeModel;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class NatsToKafkaConvertService {

    @Autowired
    private MessageService messageService;

    @Value("${veye.customer.id}")
    private Long vEyeCustomerId;

    public IntegrationModel convertFromVeyeToIot(String jsonString) throws Exception {
        IntegrationModel integrationModel = new IntegrationModel();
        try {
            VEyeModel vEyeModel = new VEyeModel();
            Gson g = new Gson();
            vEyeModel = g.fromJson(jsonString, VEyeModel.class);

            integrationModel.setExternalIdentifier(vEyeModel.getId());
            integrationModel.setPartnerName("VEYE");
            integrationModel.setActionType(vEyeModel.getActionType());
            integrationModel.setContractId(vEyeModel.getContractId() == null ? null : vEyeModel.getContractId());
            integrationModel.setDevices(vEyeModel.getData().getDevices() == null ? null : vEyeModel.getData().getDevices());
            integrationModel.setCustomerIdRequest(vEyeCustomerId);
            integrationModel.setOperators(vEyeModel.getData().getOperators());
            integrationModel.setBlockDays(vEyeModel.getData().getBlockDays());

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }

        return integrationModel;
    }

    public void sendMessageToKafka(IntegrationModel model) {
        messageService.iotQueueSendMessage(model);
    }
}
