package br.com.integrator.vEye.model;

import br.com.integrator.model.IntegrationDevice;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Data implements Serializable {
    private List<String> operators;
    private Integer blockDays;
    private List<IntegrationDevice> devices;

    public Data() {
    }

    public Data(List<String> operators, Integer blockDays, List<IntegrationDevice> devices) {
        this.operators = operators;
        this.blockDays = blockDays;
        this.devices = devices;
    }

    public List<String> getOperators() {
        return operators;
    }

    public void setOperators(List<String> operators) {
        this.operators = operators;
    }

    public Integer getBlockDays() {
        return blockDays;
    }

    public void setBlockDays(Integer blockDays) {
        this.blockDays = blockDays;
    }

    public List<IntegrationDevice> getDevices() {
        return devices;
    }

    public void setDevices(List<IntegrationDevice> devices) {
        this.devices = devices;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Data data = (Data) o;
        return Objects.equals(operators, data.operators) && Objects.equals(blockDays, data.blockDays) && Objects.equals(devices, data.devices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(operators, blockDays, devices);
    }

    @Override
    public String toString() {
        return "Data{" +
                "operators=" + operators +
                ", blockDays=" + blockDays +
                ", devices=" + devices +
                '}';
    }
}
