package br.com.integrator.vEye.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Objects;

public class VEyeModel implements Serializable {

    private String id;
    private Integer actionType;
    private String contractId;
    @JsonProperty("data")
    private Data data;

    public VEyeModel() {
    }

    public VEyeModel(String id, Integer actionType, String contractId, Data data) {
        this.id = id;
        this.actionType = actionType;
        this.contractId = contractId;
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getActionType() {
        return actionType;
    }

    public void setActionType(Integer actionType) {
        this.actionType = actionType;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VEyeModel vEyeModel = (VEyeModel) o;
        return Objects.equals(id, vEyeModel.id) && Objects.equals(actionType, vEyeModel.actionType) && Objects.equals(contractId, vEyeModel.contractId) && Objects.equals(data, vEyeModel.data);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, actionType, contractId, data);
    }

    @Override
    public String toString() {
        return "VEyeModel{" +
                "id='" + id + '\'' +
                ", actionType=" + actionType +
                ", contractId='" + contractId + '\'' +
                ", data=" + data +
                '}';
    }
}
