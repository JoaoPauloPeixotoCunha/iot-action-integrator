package br.com.integrator.vEye.controller;

import br.com.integrator.vEye.client.NatsClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NatsVeyeController {

    @Autowired
    NatsClient natsClient;

    @GetMapping("nats")
    public ResponseEntity<?> getMessage() throws InterruptedException {
        return new ResponseEntity<>("Message : " + natsClient.subscribeSync(), HttpStatus.OK);
    }

}
